﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyWeak : MonoBehaviour, IDamagable
{
    GameObject player;
    public GameObject patrolParent;
    public GameObject[] patrolSpots;

    public float timer, rayLength;
    float timerStart = 10f;

    NavMeshAgent navAgent;
    public NavMeshSurface navSurface;

    int randomSpot, maxHealth;
    public int w_health;

    enum StateSeed { idle, patrol, chase, attack}
    [SerializeField]
    StateSeed state = StateSeed.idle;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        navAgent = gameObject.GetComponent<NavMeshAgent>();
        timer = timerStart;
        randomSpot = Random.Range(0, patrolSpots.Length);
        maxHealth = w_health;

    }

    private void Update()
    {
        GetState();
        if (w_health <= 0)
            Die();

    }

    private void FixedUpdate()
    {
        StateControl();
    }

    void GetState()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit rayHit;

        if(Physics.Raycast(ray, out rayHit, rayLength))
        {
            if (rayHit.collider.CompareTag("Player") && state != StateSeed.attack)
                state = StateSeed.chase;
            else
            {
                StateTimer();
                IdleToggle();
            }
        }

    }

    void StateControl()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit rayHit;

        if (state == StateSeed.idle)
        {
            navAgent.isStopped = true;
            IdleToggle();
        }
        else if (state == StateSeed.patrol)
        {
            IdleToggle();

            if(Physics.Raycast(ray, out rayHit, 2))
            {
                if (rayHit.collider.CompareTag("Border"))
                    randomSpot = Random.Range(0, patrolSpots.Length);

            }

            navAgent.isStopped = false;

            navAgent.SetDestination(patrolSpots[randomSpot].transform.position);

            if (Vector3.Distance(transform.position, patrolSpots[randomSpot].transform.position) <= 0.2f)
                randomSpot = Random.Range(0, patrolSpots.Length);
        }
        else if (state == StateSeed.attack)
        {
            Debug.Log("Attack");
            state = StateSeed.chase;
        }
        else if (state == StateSeed.chase)
        {
            navAgent.isStopped = false;
            navAgent.SetDestination(player.transform.position);
        }

    }

    void StateTimer()
    {
        timer -= Time.fixedDeltaTime;

        if (timer < 0f)
            timer = timerStart;
    }

    void IdleToggle()
    {
        StateTimer();
        if (state == StateSeed.idle && timer <= 0.1f)
            state = StateSeed.patrol;
        else if (state == StateSeed.patrol && timer <= 0.1f)
            state = StateSeed.idle;

    }


    public void GetDamage(int damage)
    {
        w_health -= damage;
    }

    public void Die()
    {
        //could play death anim before destroying and make the enemy destroy itself after a certian period of time
        //or we could cause it to instatiate a ragdoll version of itself then destroy itself
        Destroy(gameObject);
    }

    
}
