﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public GameObject[] mapTile;
    public int randomTile;

    private void Awake()
    {
        randomTile = Random.Range(0, mapTile.Length);
        Instantiate(mapTile[randomTile], transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
