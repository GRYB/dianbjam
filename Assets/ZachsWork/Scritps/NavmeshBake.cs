﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NavmeshBake : MonoBehaviour
{
    [SerializeField]
    NavMeshSurface navSurface;

    // Start is called before the first frame update
    void Start()
    {
        navSurface.BuildNavMesh();
    }

}
