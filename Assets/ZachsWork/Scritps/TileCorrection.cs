﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCorrection : MonoBehaviour
{
    [SerializeField]
    GameObject wallGO;
    public Vector3 moveTo;

    private void Start()
    {
        StartCoroutine(DoorTrigger());
        Destroy(gameObject, 4);
    }

    private void Update()
    {
        if (wallGO != null)
            Destroy(wallGO);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Collision detected");

        if (other.gameObject.CompareTag("Wall"))
        {
            Destroy(other.gameObject);
            //Debug.Log("doorway blocked");
            if (wallGO == null)
                wallGO = other.gameObject;
        }

    }


    IEnumerator DoorTrigger()
    {
        yield return new WaitForSeconds(0.5F);

        transform.position = Vector3.Lerp(transform.position, transform.position + moveTo, 1);
        //transform.Translate(moveTo);

    }
}
