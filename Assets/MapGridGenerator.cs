﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGridGenerator : MonoBehaviour
{
    [SerializeField] private List<GameObject> tiles;
    [SerializeField] private GameObject respawnTileGO;
    [SerializeField] private int sizeX;
    [SerializeField] private int sizeY;
    [SerializeField] private float tileSize;
    [SerializeField] Vector2 respawnCoords;
    [SerializeField] private Transform mazeTransform;

    void OnEnable()
    {
        GenerateGrid();
    }
    void GenerateGrid()
    {
        for(int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                Vector3 position = new Vector3(i * tileSize, 0, j * tileSize);
                if (i == respawnCoords.x && j == respawnCoords.y)
                { 
                    Instantiate(respawnTileGO, position, Quaternion.identity, mazeTransform);
                }
                else
                {
                    Instantiate(GetRandomTile(), position, Quaternion.identity, mazeTransform);
                }
            }
        }

        GameObject GetRandomTile()
        {
            return tiles[Random.Range(0, tiles.Count)];
        }
        
    }
    

}
