﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class WeaponController : MonoBehaviour, IWeapon
{
    [SerializeField] WeaponSetting weaponSetting;
    [SerializeField] private Image crosshairImage;
    [SerializeField] private Transform weaponParentTransform;
    [SerializeField] private Camera camera;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private List<WeaponSetting> weaponSettings;
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
            Fire();
    }
    
    void Start()
    {
        if (weaponSettings.Count != 0)
            //replace with weapon settings from levelSettings
            Setup(weaponSettings[Random.Range(0,weaponSettings.Count)]);
    }

    void Setup(WeaponSetting weaponSetting)
    {
        crosshairImage.sprite = weaponSetting.crosshairImg;
        audioSource.clip = weaponSetting.fireSound;
        GameObject go = Instantiate(weaponSetting.weaponGO, Vector3.zero, Quaternion.identity);
        go.transform.SetParent(weaponParentTransform, false);
        go.transform.localScale =  Vector3.one;

    }

    public  void Fire()
    {
        Debug.Log("fire");
        audioSource.Play();
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, weaponSetting.range))
        {
            Debug.Log("hit");

            IDamagable target = hit.transform.GetComponent<IDamagable>();
            if(target != null)
            {
                Debug.Log("hit idamagable");
                target.GetDamage(weaponSetting.damage);
            }    
            else
            {
                Debug.Log("hit idamagable");
            }
        }
    }

    public void Reload()
    {
        
    }
}
