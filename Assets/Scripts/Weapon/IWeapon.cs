﻿interface IWeapon
{
     void Fire();
     void Reload();  
}
