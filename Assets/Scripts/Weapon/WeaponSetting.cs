﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "New weapon")]
public class WeaponSetting : ScriptableObject
{
    public int damage;
    public int range;
    public bool autoMode;
    public int fireRate;
    public int maxAmo;
    public int reloadTime;
    public float recoil;
    //public Mesh instead
    //todo: fire sound settings
    public AudioClip fireSound;
    public GameObject weaponGO;
    public Sprite crosshairImg;

}
