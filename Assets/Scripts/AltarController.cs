﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarController : MonoBehaviour, IDamagable
{
    //public int HP { get; private set; }
    public int HP = 50;
    public void GetDamage(int damage)
    {
        Debug.Log("get damage");

        HP -= damage;
        if (HP <= 0)
            Die();
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}
